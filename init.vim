"vim-plug plugins
call plug#begin('~/.local/share/nvim/plugged')

Plug 'vim-airline/vim-airline' "lean & mean status/tabline for vim that's light as air

Plug 'airblade/vim-gitgutter' "A Vim plugin which shows a git diff in the 'gutter'

Plug 'drewtempelmeyer/palenight.vim' "Soothing color scheme

Plug 'junegunn/fzf' "a general-purpose command-line fuzzy finder.
Plug 'junegunn/fzf.vim' "fzf <3 vim

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

Plug 'jbgutierrez/vim-better-comments' "Easily highlight human-friendly comments in your code!

" LanguageClient
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }
" (Optional) Multi-entry selection UI.
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
let g:deoplete#enable_at_startup = 1

call plug#end()

" Required for operations modifying multiple buffers like rename.
set hidden

" Language protocols
let g:LanguageClient_serverCommands = {
    \ 'rust': ['~/.cargo/bin/rustup', 'run', 'stable', 'rls'],
    \ 'ruby': [&shell, &shellcmdflag, 'solargraph stdio'],
    \ 'java': ['~/bin/jdtls', '-data', getcwd()],
    \ 'python': ['/usr/bin/pyls'],
    \ 'cs': ['~/.omnisharp/omnisharp-roslyn/run'],
    \ }

" LanguageClient mappings
nnoremap <F5> :call LanguageClient_contextMenu()<CR>
nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

"Use the Silver Searcher for grepping.
"This searcher is fast and respects the .gitignore file.
" * to find word under cursor in current file
" <leader>* to search for the word under cursor in the project
" Look up files with FZF instead of CtrlP.
if executable('ag')
  set grepprg=ag\ --nogroup\ --nocolor
endif
set rtp+=~/.fzf
nnoremap <Leader>* :Ag <C-R><C-W>
nmap  <silent> <Leader>* :Ag <cword><CR>
map <C-p> :Files<CR>

set background=dark
colorscheme palenight

set tabstop=4
set number "showing line numbers
set cc=80,130 "having a right margin in the 80th and 130th column
set list "showing whitespaces
set maxmempattern=100000

" (neo)mutt files should not have longer lines than 72
au BufRead /tmp/*mutt-* set tw=72

